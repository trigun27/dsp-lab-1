﻿namespace DSP_rectangle_Gausse
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.GrafTabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checVisualOrNot = new System.Windows.Forms.CheckBox();
            this.squareWin = new System.Windows.Forms.Button();
            this.squareDelt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.squareT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SquareChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.GaussWin = new System.Windows.Forms.Button();
            this.txtDeltaT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSigma = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAmplituda = new System.Windows.Forms.TextBox();
            this.GaussChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.GrafTabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).BeginInit();
            this.SuspendLayout();
            // 
            // GrafTabs
            // 
            this.GrafTabs.Controls.Add(this.tabPage1);
            this.GrafTabs.Controls.Add(this.tabPage2);
            this.GrafTabs.Location = new System.Drawing.Point(12, 12);
            this.GrafTabs.Name = "GrafTabs";
            this.GrafTabs.SelectedIndex = 0;
            this.GrafTabs.Size = new System.Drawing.Size(1098, 660);
            this.GrafTabs.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checVisualOrNot);
            this.tabPage1.Controls.Add(this.squareWin);
            this.tabPage1.Controls.Add(this.squareDelt);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.squareT);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.SquareChart);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1090, 634);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Прямоугольный сигнал";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checVisualOrNot
            // 
            this.checVisualOrNot.AutoSize = true;
            this.checVisualOrNot.Location = new System.Drawing.Point(918, 180);
            this.checVisualOrNot.Name = "checVisualOrNot";
            this.checVisualOrNot.Size = new System.Drawing.Size(158, 17);
            this.checVisualOrNot.TabIndex = 6;
            this.checVisualOrNot.Text = "Скрыть восстановленный";
            this.checVisualOrNot.UseVisualStyleBackColor = true;
            this.checVisualOrNot.CheckedChanged += new System.EventHandler(this.checVisualOrNot_CheckedChanged);
            // 
            // squareWin
            // 
            this.squareWin.Location = new System.Drawing.Point(918, 139);
            this.squareWin.Name = "squareWin";
            this.squareWin.Size = new System.Drawing.Size(75, 23);
            this.squareWin.TabIndex = 5;
            this.squareWin.Text = "Посчитать";
            this.squareWin.UseVisualStyleBackColor = true;
            this.squareWin.Click += new System.EventHandler(this.squareWin_Click);
            // 
            // squareDelt
            // 
            this.squareDelt.Location = new System.Drawing.Point(917, 96);
            this.squareDelt.Name = "squareDelt";
            this.squareDelt.Size = new System.Drawing.Size(100, 20);
            this.squareDelt.TabIndex = 4;
            this.squareDelt.Text = "0.1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(914, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Δt";
            // 
            // squareT
            // 
            this.squareT.Location = new System.Drawing.Point(917, 48);
            this.squareT.Name = "squareT";
            this.squareT.Size = new System.Drawing.Size(100, 20);
            this.squareT.TabIndex = 2;
            this.squareT.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(914, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "T";
            // 
            // SquareChart
            // 
            chartArea1.Name = "ChartArea1";
            this.SquareChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.SquareChart.Legends.Add(legend1);
            this.SquareChart.Location = new System.Drawing.Point(6, 6);
            this.SquareChart.Name = "SquareChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series1.Color = System.Drawing.Color.Lime;
            series1.Legend = "Legend1";
            series1.Name = "Original";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.Red;
            series2.Legend = "Legend1";
            series2.Name = "Restored";
            this.SquareChart.Series.Add(series1);
            this.SquareChart.Series.Add(series2);
            this.SquareChart.Size = new System.Drawing.Size(905, 625);
            this.SquareChart.TabIndex = 0;
            this.SquareChart.Text = "chart1";
            this.SquareChart.UseWaitCursor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBox1);
            this.tabPage2.Controls.Add(this.GaussWin);
            this.tabPage2.Controls.Add(this.txtDeltaT);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtSigma);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txtAmplituda);
            this.tabPage2.Controls.Add(this.GaussChart);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1090, 634);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Сигнал Гаусса";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(928, 226);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(158, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Скрыть восстановленный";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // GaussWin
            // 
            this.GaussWin.Location = new System.Drawing.Point(928, 167);
            this.GaussWin.Name = "GaussWin";
            this.GaussWin.Size = new System.Drawing.Size(75, 23);
            this.GaussWin.TabIndex = 9;
            this.GaussWin.Text = "Рассчитать";
            this.GaussWin.UseVisualStyleBackColor = true;
            this.GaussWin.Click += new System.EventHandler(this.GaussWin_Click);
            // 
            // txtDeltaT
            // 
            this.txtDeltaT.Location = new System.Drawing.Point(928, 130);
            this.txtDeltaT.Name = "txtDeltaT";
            this.txtDeltaT.Size = new System.Drawing.Size(100, 20);
            this.txtDeltaT.TabIndex = 8;
            this.txtDeltaT.Text = "0.01";
            this.txtDeltaT.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(925, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Δt";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtSigma
            // 
            this.txtSigma.Location = new System.Drawing.Point(928, 82);
            this.txtSigma.Name = "txtSigma";
            this.txtSigma.Size = new System.Drawing.Size(100, 20);
            this.txtSigma.TabIndex = 6;
            this.txtSigma.Text = "0.5";
            this.txtSigma.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(925, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "σ";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(925, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "A";
            // 
            // txtAmplituda
            // 
            this.txtAmplituda.Location = new System.Drawing.Point(928, 35);
            this.txtAmplituda.Name = "txtAmplituda";
            this.txtAmplituda.Size = new System.Drawing.Size(100, 20);
            this.txtAmplituda.TabIndex = 2;
            this.txtAmplituda.Text = "1";
            // 
            // GaussChart
            // 
            chartArea2.Name = "ChartArea1";
            this.GaussChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.GaussChart.Legends.Add(legend2);
            this.GaussChart.Location = new System.Drawing.Point(3, 3);
            this.GaussChart.Name = "GaussChart";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series3.Color = System.Drawing.Color.Lime;
            series3.Legend = "Legend1";
            series3.Name = "Original";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Color = System.Drawing.Color.Red;
            series4.Legend = "Legend1";
            series4.Name = "Restored";
            this.GaussChart.Series.Add(series3);
            this.GaussChart.Series.Add(series4);
            this.GaussChart.Size = new System.Drawing.Size(898, 625);
            this.GaussChart.TabIndex = 1;
            this.GaussChart.Text = "chart1";
            this.GaussChart.UseWaitCursor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 684);
            this.Controls.Add(this.GrafTabs);
            this.Name = "Form1";
            this.Text = "Form1";
            this.GrafTabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl GrafTabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox squareDelt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox squareT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart SquareChart;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button squareWin;
        private System.Windows.Forms.DataVisualization.Charting.Chart GaussChart;
        private System.Windows.Forms.TextBox txtDeltaT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSigma;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAmplituda;
        private System.Windows.Forms.Button GaussWin;
        private System.Windows.Forms.CheckBox checVisualOrNot;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

