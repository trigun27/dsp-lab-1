﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DSP_rectangle_Gausse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        void RunPulse(Pulse pulse, Chart chart, double period, double delta)
        {
            var sample = pulse.Sample(delta).ToList();
            var restoredPoints = Pulse.KotelnikovFormula(sample, period, delta);
            AddPoints(chart.Series["Original"],pulse.DataPoints());
            AddPoints(chart.Series["Restored"], restoredPoints.DataPoints());
        }

        void AddPoints(Series series, IEnumerable<PulsePoint> points)
        {
            series.Points.Clear();
            foreach (var pulsePoint in points)
            {
                series.Points.AddXY(pulsePoint.X, pulsePoint.Y);
            }
        }

        private void GaussWin_Click(object sender, EventArgs e)
        {
            double amplituda = Convert.ToDouble(txtAmplituda.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            double sigma = Convert.ToDouble(txtSigma.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            double delT = Convert.ToDouble(txtDeltaT.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            double period = 3*sigma;
            RunPulse(Pulse.CreateGausseSiganl(amplituda, sigma), GaussChart, period, delT);
        }

        private void squareWin_Click(object sender, EventArgs e)
        {
            double delT = Convert.ToDouble(squareDelt.Text.Replace(",", "."), CultureInfo.InvariantCulture );
            double period = Convert.ToDouble(squareT.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            RunPulse(SquarePulse.CreateSquarePulse(period),SquareChart,period,delT);
        }

        private void checVisualOrNot_CheckedChanged(object sender, EventArgs e)
        {
            if (checVisualOrNot.Checked)
                try
                {
                    SquareChart.Series["Restored"].Enabled = false;
                   
                }
                catch (Exception)
                {

                    throw;
                }
            else
            {
                SquareChart.Series["Restored"].Enabled = true;
         
            }
        }


        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                try
                {
                   
                    GaussChart.Series["Restored"].Enabled = false;
                }
                catch (Exception)
                {

                    throw;
                }
            else
            {       
                GaussChart.Series["Restored"].Enabled = true;
            }
        }

     
       

 

      
    }
}
