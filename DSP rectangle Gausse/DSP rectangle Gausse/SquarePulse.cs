﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSP_rectangle_Gausse
{
    public class SquarePulse: Pulse
    {

        private SquarePulse(Func<double, double> pulseFunc, double period) : base(pulseFunc, period)
        {
        }


        public static SquarePulse CreateSquarePulse(double period)
        {
            return new SquarePulse(CreateSquarePulseFunc(period), period);
        }

        private static Func<double, double> CreateSquarePulseFunc(double period)
        {
            return x =>
            {
                if ((x > -period) && (x < period))
                    return 1;
                if ((x == -period) && (x == period))
                    return 0.5;
                return 0;
            };
        }

        public override IEnumerable<PulsePoint> DataPoints()
        {
            return new List<PulsePoint> { new PulsePoint(-2 * Period, 0), new PulsePoint(-Period, 0), new PulsePoint(-Period, 1), new PulsePoint(Period, 1), new PulsePoint(Period, 0), new PulsePoint(2 * Period, 0) };
        }



    }
}
