﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSP_rectangle_Gausse
{
    public class PulsePoint
    {
        private readonly double _x;
        private readonly double _y;

        public PulsePoint(double x, double y)
        {
            _x = x;
            _y = y;
        }

        public double X { get { return _x; } }
        public double Y { get { return _y; } }
    }
}
