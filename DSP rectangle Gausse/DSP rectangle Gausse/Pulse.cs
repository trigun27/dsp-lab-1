﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSP_rectangle_Gausse
{
    public class Pulse
    {
        protected const int N = 1000;
        private readonly double _period;
        private readonly Func<double, double> _pulseFunc;

        private static double Sinc(double x)
        {
            return Math.Sin(x)/x;
        }


        /// <summary>
        /// Восстанавливает исходный сигнал
        /// </summary>
        /// <param name="points">Точки оригинальной функции</param>
        /// <param name="period">период</param>
        /// <param name="deltat">шаг дескритизации</param>
        /// <returns></returns>
        public static Pulse KotelnikovFormula(IEnumerable<PulsePoint> points, double period, double deltat)
        {
            var lowIndex = - points.Count()/2;
            Func<double, double> func = x => points.Select( (p,i) => p.Y*Sinc((Math.PI/deltat)*(x - (i+lowIndex)*deltat)) ).Sum();
            return new Pulse(func, period);
        }

        /// <summary>
        /// оригинальный сигнал Гаусса
        /// </summary>
        /// <param name="amplitude">Амплитуда</param>
        /// <param name="sigma">сигма</param>
        /// <returns></returns>
        public static Pulse CreateGausseSiganl(double amplitude, double sigma)
        {
            return new Pulse(x => amplitude*Math.Exp(-Math.Pow(x/sigma, 2)), 3*sigma);
        }


        public Pulse(Func<double,double> pulseFunc,double period)
        {
            _pulseFunc = pulseFunc;
            _period = period;
        }

        protected double Period { get { return _period; } }

        public double Y(double x)
        {
            return _pulseFunc(x);
        }

        public IEnumerable<PulsePoint> Sample(double delta)
        {
            var current = Period*-2;
            while (current <= 2*Period)
            {
                yield return   new PulsePoint(current, _pulseFunc(current));
                current += delta;
            }
        }

        public virtual IEnumerable<PulsePoint> DataPoints()
        {
            var delta = 4*Period/N;
            return Sample(delta);
        }

    }
}
